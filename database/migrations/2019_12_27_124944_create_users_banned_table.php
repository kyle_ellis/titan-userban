<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersBannedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_banned', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->morphs('bannable');
            $table->string('reason', 255);
            $table->date('ban_until')->nullable();
            $table->boolean('forever')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_banned');
    }
}
