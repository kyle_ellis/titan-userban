<?php
/**
 * Created by PhpStorm.
 * User: kyle
 * Date: 2019-12-27
 * Time: 04:43
 */

namespace Extensions\Kylemassacre\Userban\Http\Middleware;

use Closure;
use Extensions\Kylemassacre\Userban\Model\UserBanned;
use Illuminate\Database\Eloquent\Builder;


class UserBannedMiddleware
{

    public function handle($request, Closure $next, $guard = null)
    {
        if(auth()->check() && !in_array($request->route()->getName(), config('userban.excluded_routes')))
        {
            $isBanned = UserBanned::where(function(Builder $query) {
                $query->where('bannable_id', auth()->user()->id);
                $query->where('bannable_type', get_class(auth()->user()));
            })
            ->orWhere(function(Builder $query) {
                if(auth()->user()->character != null)
                {
                    $query->where('bannable_id', auth()->user()->character->id);
                    $query->where('bannable_type', get_class(auth()->user()->character));
                }
            })->first();
            if($isBanned)
            {
                if($isBanned->forever || now()->diffInDays($isBanned->ban_until) >= 1)
                {
                    return redirect()->route('userban.userbanned');
                }
                else
                {
                    $isBanned->delete();
                    return $next($request);
                }
            }
            else
            {
                return $next($request);
            }
        } else
        {
            return $next($request);
        }
    }
}
