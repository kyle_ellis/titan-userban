<?php
/**
 * Created by PhpStorm.
 * User: kyle
 * Date: 2019-12-26
 * Time: 04:07
 */

namespace Extensions\Kylemassacre\Userban\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class UserbanRequest extends FormRequest
{

    public function authorize()
    {
//        return true;
        return \Auth::user()->can('ban-user');
    }

    public function rules()
    {

        return [
            'user_id' => 'exists:users,id|required',
            'forever' => 'required_without:ban_until',
            'reason' => 'required|string|filled|min:5|max:255',
            'ban_until' => 'required_without:forever|date|after:today|nullable',
        ];
    }


}
