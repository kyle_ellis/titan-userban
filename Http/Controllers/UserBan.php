<?php

namespace Extensions\Kylemassacre\Userban\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use Extensions\Kylemassacre\Userban\Model\UserBanned;

class UserBan extends Controller
{
    public function __construct()
    {
        $this->middleware('user_banned');

    }

    public function index()
    {
        $type = null;
        $ban = UserBanned::where(function(Builder $query) use (&$type) {
            $query->where('bannable_id', auth()->user()->id);
            $query->where('bannable_type', get_class(auth()->user()));
            $type = 'User';
        })
            ->orWhere(function(Builder $query) use (&$type) {
                if(auth()->user()->character != null)
                {
                    $query->where('bannable_id', auth()->user()->character->id);
                    $query->where('bannable_type', get_class(auth()->user()->character));
                    $type = 'Character';
                }
            })->first();
        return view('userban::game.userban', compact('ban', 'type'));
    }

}
