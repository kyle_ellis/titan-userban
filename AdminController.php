<?php

namespace Extensions\Kylemassacre\Userban;

use App\User;
use Illuminate\View\View;
use App\Http\Controllers\Controller;
use Extensions\Kylemassacre\Userban\Facades\BanUser;
use Extensions\Kylemassacre\Userban\Model\UserBanned;
use Extensions\Kylemassacre\Userban\Http\Requests\UserbanRequest;

class AdminController extends Controller
{

    public function index(): View
    {

        $users = User::all();

        return view('userban::admin.index', compact('users'));
    }

    public function userBan(UserbanRequest $request)
    {
        $bannedUser = BanUser::setUser(User::find($request->user_id))
            ->setReason($request->reason)
            ->setBanUntil($request->ban_until)
            ->setForever($request->forever == 'on');

        if($bannedUser->placeBan())
        {
            flash()->success($bannedUser->getUser()->name .' was banned');
            return redirect()->back()->withInput();
        }
        else
        {
            flash()->error('There was an error banning that user');
            return redirect()->back()->withInput();
        }


    }

}
