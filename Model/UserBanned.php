<?php
/**
 * Created by PhpStorm.
 * User: kyle
 * Date: 2019-12-28
 * Time: 06:28
 */

namespace Extensions\Kylemassacre\Userban\Model;


use App\User;
use Illuminate\Database\Eloquent\Model;

class UserBanned extends Model
{
    protected $guarded = ['id'];

    protected $table = 'users_banned';

    protected $casts = [
        'forever' => 'boolean'
    ];

    protected $dates = [
        'ban_until'
    ];

    public function bannable()
    {
        return $this->morphTo();
    }

}
