<?php

Route::group([
    'namespace' => 'Extensions\Kylemassacre\Userban',
    'middleware' => [
        'web'
    ]
], function () {

    include __DIR__ . '/routes/game.php';

    include __DIR__ . '/routes/admin.php';

});
