@extends('titan::layouts.app')

@section('content')
<div class="container-fluid">
    <h3>Your {{ $type }} is banned</h3>

    @if($ban->forever)
        <p>You are permanently banned</p>
    @else
        <p>Your are banned for {{ now()->diffInDays($ban->ban_until) }} more {{ \Str::plural('day', now()->diffInDays($ban->ban_until)) }}</p>
    @endif
    <p class="text-muted"><b>Reason: </b>{{ $ban->reason }}</p>
</div>
@endsection
