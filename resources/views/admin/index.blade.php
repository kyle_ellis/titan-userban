@extends('titan::layouts.admin')

@section('page')
    <div class="card shadow mb-4">
        {!! Form::open()->route('admin.userban.ban')->post() !!}
        @include('userban::admin.partials.ban_user_form')
    </div>

    <div class="card shadow">
        <div class="card-body">
            {!! Form::submit("Ban")->danger() !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('#inp-user_id').select2({
                'minimumInputLength': 2
            });
        });
    </script>
@endpush
