<div class="card-body">
    <h2>{{ config('userban.extension_name') }}</h2>
    <div class="form-group">
        {!! Form::select('user_id', 'Select User')
            ->value(old('user_id'))
            ->options($users, 'name', 'id')
            ->help('Select the user to ban') !!}
        {!! Form::text('reason', 'Reason for ban')
            ->value(old('reason'))
            ->help('Enter a reason for banning the user')!!}
        {!! Form::date('ban_until', 'How Long')
            ->value(old('ban_until'))
            ->help('When should they be banned until?') !!}
        {!! Form::checkbox('forever', 'Forever')
            ->checked(old('forever') == 'on') !!}
    </div>
</div>
