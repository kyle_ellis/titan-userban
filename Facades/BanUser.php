<?php

namespace Extensions\Kylemassacre\Userban\Facades;

use Extensions\Kylemassacre\Userban\BanUserManager;
use Illuminate\Support\Facades\Facade;

class BanUser extends Facade
{
    protected static function getFacadeAccessor()
    {
        return BanUserManager::class;
    }
}
