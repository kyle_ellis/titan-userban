<?php

namespace Extensions\Kylemassacre\Userban;


use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Model;
use Extensions\Kylemassacre\Userban\Traits\Bannable;
use Extensions\Kylemassacre\Userban\Model\UserBanned;

class BanUserManager
{
    private $user;

    private $forever;

    private $banUntil;

    private $reason;

    private $type;

    public function __construct()
    {

    }

    /**
     * @return \App\User $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param Model $user
     * @return BanUserManager
     */
    public function setUser(Model $user)
    {
        $this->user = $user;
        $this->setType($user);

        return $this;
    }

    /**
     * @return boolean
     */
    public function getForever()
    {
        return $this->forever;
    }

    /**
     * @param boolean $forever
     * @return BanUserManager
     */
    public function setForever($forever)
    {
        $this->forever = $forever;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBanUntil()
    {
        return $this->banUntil;
    }

    /**
     * @param \DateTime $banUntil
     * @return BanUserManager
     */
    public function setBanUntil($banUntil)
    {
        $this->banUntil = $banUntil;

        return $this;
    }

    /**
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     * @return BanUserManager
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * @return Bannable
     */
    public function getType()
    {
        return get_class($this->getUser());
    }

    /**
     * @param Model $type
     * @return BanUserManager
     */
    public function setType(Model $type)
    {
        $this->type = get_class($type);

        return $this;
    }

    /**
     * @return bool
     */
    public function isBanned()
    {
        $search = UserBanned::where(function(Builder $query) {
            $query->where('bannable_id', $this->getUser()->id);
            $query->where('bannable_type', $this->getType());
        })->exists();

        return $search;
    }

    /**
     * @return bool|UserBanned|\Illuminate\Database\Eloquent\Model
     */
    public function placeBan()
    {
        $bannedUser = UserBanned::updateOrCreate(
            ['bannable_id' => $this->getUser()->id, 'bannable_type' => $this->getType()],
            ['reason' => $this->getReason(),
                'ban_until' => $this->getBanUntil(),
                'forever' => $this->getForever()]);

        if ($bannedUser->exists()) {
            return $bannedUser;
        } else {
            return false;
        }
    }

    /**
     * @return boolean
     * @throws \Exception
     */
    public function removeBan()
    {
        return UserBanned::where(function(Builder $query) {
          $query->where('bannable_id', $this->getUser()->id);
          $query->where('bannable_type', $this->getType());
        })->delete();
    }

    /**
     * @param string|null $type
     * @return \Illuminate\Support\Collection
     */
    public function getUsersBanned(string $type = null)
    {

        $banned = UserBanned::with('bannable');
        if($type != null) {
            $banned->where('bannable_type', '=', $type);
        }
        $banned->get();
        return collect($banned->get());
    }
}
