<?php
use \Illuminate\Support\Facades\Route;

Route::group([
    'namespace' => 'Http\Controllers',
    'middleware' => ['auth']
], function (\Illuminate\Routing\Router $route) {
    $route->get('banned', 'UserBan@index')->name('userban.userbanned');
});
