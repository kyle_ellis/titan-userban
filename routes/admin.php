<?php
use \Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => ['auth', 'permission:ban-user', 'update_last_move'],
    'prefix' => 'admin'
], function (\Illuminate\Routing\Router $route) {
    $route->get('/userban', 'AdminController@index')->name('admin.userban.index');
    $route->post('/userban', 'AdminController@userBan')->name('admin.userban.ban');
});
