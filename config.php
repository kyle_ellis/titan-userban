<?php

return [
    'extension_name' => 'User Ban',
    'excluded_routes' => [
        // DO NOT REMOVE
        'logout', 'userban.userbanned',
        // END DO NOT REMOVE. ADD BELOW THIS LINE
    ]
];
