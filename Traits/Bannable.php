<?php

namespace Extensions\Kylemassacre\Userban\Traits;

use Extensions\Kylemassacre\Userban\Model\UserBanned;

/** @mixin \Eloquent */

trait Bannable
{

    public function banned()
    {
        return $this->morphOne(UserBanned::class, 'bannable');
    }

    public function charactersBanned()
    {
        return $this->morphMany(UserBanned::class, 'bannable');
    }

    public function placeBan(string $reason, \DateTime $time = null, bool $forever = false)
    {
        return \BanUser::setBanUntil($time)
            ->setUser($this)
            ->setReason($reason)
            ->setForever($forever)
            ->placeBan();
    }

}
