<?php
namespace Extensions\Kylemassacre\Userban;

use Extensions\Kylemassacre\Userban\Facades\BanUser;
use Illuminate\Contracts\Support\DeferrableProvider;
use Extensions\Kylemassacre\Userban\Http\Middleware\UserBannedMiddleware;

class ServiceProvider extends \Illuminate\Support\ServiceProvider implements DeferrableProvider
{

    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'userban');
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');

        $this->publishResources();
        app('router')->aliasMiddleware('user_banned', UserBannedMiddleware::class);
        app('router')->pushMiddlewareToGroup('web', UserBannedMiddleware::class);

    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config.php', 'userban');

        app()->bind(BanUserManager::class, function() {
            return new BanUserManager();
        });
    }

    private function publishResources()
    {
        $this->publishes([
            __DIR__.'/config.php' => config_path('userban.php')
        ], ['userban-config', 'userban-all']);

        $this->publishes([
            __DIR__.'/resources/views' => base_path('resources/views/vendor/userban'),
        ], ['userban-views', 'userban-all']);
    }

    public function provides()
    {
        return [BanUserManager::class];
    }

}
