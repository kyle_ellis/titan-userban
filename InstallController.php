<?php
namespace Extensions\Kylemassacre\Userban;

class InstallController {
    public function index() {
    }

    public function install()
    {
        try {
            \Artisan::call('migrate', [
                '--realpath' => './database/migrations'
            ]);

            flash()->success('Migrations Complete');
            return redirect()->back();

        } catch (\Exception $exception) {
            flash()->error('There was an error migrating. Error Code ['.$exception->getCode().']');
            return redirect()->back();
        }
    }

    public function uninstall()
    {
        try {
            \DB::transaction(function() {
                \DB::table('migrations')
                    ->where([
                        ['migration', '=', '2019_12_27_124944_create_users_banned_table']
                    ])->delete();
                \Schema::dropIfExists('users_banned');
            });

            flash()->success('Migrations successfully rolled back');
            return redirect()->back();
        } catch (\Throwable $throwable) {
            flash()->error('There was an error migrating. Error Code ['.$throwable->getMessage().']');
            return redirect()->back();
        }

    }


}
